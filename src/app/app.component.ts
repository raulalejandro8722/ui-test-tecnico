import { Component, OnInit } from '@angular/core';
import {NotificationService} from '@app/services';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'client-encuesta-app';

  constructor(
    private notification: NotificationService
    ){}

  ngOnInit(){
  }

  onSuccess() : void {
    this.notification.success("El procedimiento fue exitoso");
  }

  onError() : void {
    this.notification.error("Se encontraron errores en el proceso");
  }


}
