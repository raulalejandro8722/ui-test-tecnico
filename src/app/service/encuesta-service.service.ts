import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class EncuestaService{
    constructor(private http:HttpClient) { }

    resultadoEncuestas(){
      return this.http.get('http://localhost:6565/api/encuesta/getAll')
    }

    responderEncuesta(encuesta: any){
      return this.http.post('http://localhost:6565/api/encuesta/saveEncuesta', encuesta)
    }

}

