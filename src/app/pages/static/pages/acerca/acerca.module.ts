import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcercaRoutingModule } from './acerca-routing.module';
import { MatCardModule } from '@angular/material/card';
import { AcercaComponent } from './acerca.component';


@NgModule({
  declarations: [
    AcercaComponent
  ],
  imports: [
    CommonModule,
    AcercaRoutingModule,
    MatCardModule
  ]
})
export class AcercaModule { }
