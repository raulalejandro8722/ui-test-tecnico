import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EncuestaService } from '@app/service/encuesta-service.service';
import { NotificationService } from '@app/services/notification/notification.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  encuesta: any;
  constructor(
    private encuestaService: EncuestaService,
    private notification: NotificationService
  ) { }

  ngOnInit(): void {
  }

  registrarEncuesta(form: NgForm) {
    console.log(form)
    if(form.valid){
      this.encuesta = {
          email: form.value.email,
          estiloMusical: form.value.estiloMusical
      }
    }
    console.log(this.encuesta)
    this.encuestaService.responderEncuesta(this.encuesta).subscribe()
    this.notification.success("El procedimiento fue exitoso");
  }


}
