import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EncuestaService } from '@app/service/encuesta-service.service';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss']
})
export class ResultadosComponent implements OnInit {
  resultadosEncuesta : any = [];
  dataSource : any;
  cantidadVotos: any;
  displayedColumns: string[] = ['estiloMusical'];

  constructor(private encuestaService: EncuestaService) { }

  ngOnInit(){
    this.consultarResultadosEncuesta();
  }

  consultarResultadosEncuesta(){
    this.encuestaService.resultadoEncuestas().subscribe(data =>{
        this.resultadosEncuesta = data;
        if(this.resultadosEncuesta[1].estiloMusical == 'Rock'){
          console.log("jeje")
        }
        this.dataSource = new MatTableDataSource(this.resultadosEncuesta)
        console.log(this.resultadosEncuesta)
    })
  }

}
