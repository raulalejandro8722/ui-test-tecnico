import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then(m=> m.WelcomeModule)
  },
  {
    path: '404',
    loadChildren: () => import('./pages/not-found/not-found.module').then(m=> m.NotFoundModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./pages/registration/registration.module').then(m=> m.RegistrationModule)
  }
  ,
  {
    path: 'acerca',
    loadChildren: () => import('./pages/acerca/acerca.module').then(m=> m.AcercaModule)
  }
  ,
  {
    path: 'resultados',
    loadChildren: () => import('./pages/resultados/resultados.module').then(m=> m.ResultadosModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule { }
